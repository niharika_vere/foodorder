package com.foodie.service;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.foodie.dto.FoodItemsDto;
import com.foodie.dto.ResponseDto;
import com.foodie.entity.FoodItems;
import com.foodie.repository.FoodItemsRepository;
import com.foodie.serviceImpl.FoodItemsServiceImpl;
 
@ExtendWith(MockitoExtension.class)
 class FoodItemsServiceImplTest {
 
    @InjectMocks
    private FoodItemsServiceImpl foodItemsService;
 
    @Mock
    private FoodItemsRepository foodItemsRepository;
 
    @Test
     void testAddItems() {
        
        FoodItemsDto foodItemsDto = new FoodItemsDto();
        ResponseDto responseDto = foodItemsService.addItems(foodItemsDto);
 
        verify(foodItemsRepository, times(1)).save(any(FoodItems.class));
        assertEquals(201, responseDto.getHttpCode());
        assertEquals("Items Added Successfully", responseDto.getMessage());
    }
}