package com.foodie.service;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.foodie.dto.ResponseDto;
import com.foodie.dto.UserDto;
import com.foodie.entity.User;
import com.foodie.exceptions.UserAlreadyExists;
import com.foodie.repository.UserRepository;
import com.foodie.serviceImpl.UserServiceImpl;
 
@SpringBootTest
class UserServiceImplTest {
 
    @Mock
    private UserRepository userRepository;
 
    @InjectMocks
    private UserServiceImpl userService;
 
    @Test
    void testRegister_SuccessfulRegistration() {
        UserDto userDto = new UserDto("Niharika", "niharika@gmail.com", "1234567890");
 
        when(userRepository.findByEmail(userDto.getEmail())).thenReturn(Optional.empty());
 
        ResponseDto responseDto = userService.register(userDto);
 
        assertEquals(201, responseDto.getHttpCode());
        assertEquals("Registered successfully", responseDto.getMessage());
        verify(userRepository, times(1)).save(any());
    }
 
    @Test
    void testRegister_UserAlreadyExists() {
        UserDto userDto = new UserDto("Keerthika", "keerthika@gmail.com", "1234567890");
 
        when(userRepository.findByEmail(userDto.getEmail())).thenReturn(Optional.of(new User()));
 
        assertThrows(UserAlreadyExists.class, () -> userService.register(userDto));
        verify(userRepository, never()).save(any());
    }
 

}

