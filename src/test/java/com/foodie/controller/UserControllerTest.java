package com.foodie.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.foodie.dto.ResponseDto;
import com.foodie.dto.UserDto;
import com.foodie.serviceImpl.UserServiceImpl;
 
@ExtendWith(MockitoExtension.class)
class UserControllerTest {
 
    @Mock
    private UserServiceImpl userServiceImpl;
 
    @InjectMocks
    private UserController userController;
 
    @Test
    void testRegister_SuccessfulRegistration() {
        UserDto userDto = new UserDto("Niharika", "niha@gmail.com", "1234567890");
        ResponseDto expectedResponse = ResponseDto.builder().httpCode(201).message("Registered successfully").build();
 
        when(userServiceImpl.register(userDto)).thenReturn(expectedResponse);
 
        ResponseEntity<ResponseDto> responseEntity = userController.register(userDto);
 
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }
  
}
