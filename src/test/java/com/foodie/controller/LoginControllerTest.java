package com.foodie.controller;
import com.foodie.dto.LoginDto;
import com.foodie.dto.ResponseDto;
import com.foodie.service.UserService;
import com.foodie.service.VendorService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
 
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
 
class LoginControllerTest {
 
    @Mock
    private UserService userService;
 
    @Mock
    private VendorService vendorService;
 
    @InjectMocks
    private LoginController loginController;
 
    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }
 
    @Test
    void login_shouldCallVendorService_whenIdStartsWithVID() {
        LoginDto loginDto = new LoginDto("VID123", "password");
 
        when(vendorService.login(loginDto)).thenReturn(new ResponseDto());
 
        ResponseEntity<ResponseDto> responseEntity = loginController.login(loginDto);
 
        verify(vendorService, times(1)).login(loginDto);
        verify(userService, never()).login(loginDto);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
 
    @Test
    void login_shouldCallUserService_whenIdStartsWithCID() {
        LoginDto loginDto = new LoginDto("CID456", "password");
 
        when(userService.login(loginDto)).thenReturn(new ResponseDto());
 
        ResponseEntity<ResponseDto> responseEntity = loginController.login(loginDto);
 
        verify(userService, times(1)).login(loginDto);
        verify(vendorService, never()).login(loginDto);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
 
    @Test
    void login_shouldReturnBadRequest_whenIdDoesNotStartWithVIDorCID() {
        LoginDto loginDto = new LoginDto("InvalidId", "password");
 
        ResponseEntity<ResponseDto> responseEntity = loginController.login(loginDto);
 
        verify(vendorService, never()).login(loginDto);
        verify(userService, never()).login(loginDto);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals("Invalid login request", responseEntity.getBody().getMessage());
    }
}
