package com.foodie.service;

import com.foodie.dto.LoginDto;
import com.foodie.dto.ResponseDto;

import jakarta.validation.Valid;

public interface LoginService {
	 ResponseDto login(@Valid LoginDto loginDto);
}
