package com.foodie.service;

import com.foodie.dto.LoginDto;
import com.foodie.dto.ResponseDto;
import com.foodie.dto.UserDto;

import jakarta.validation.Valid;

public interface UserService {
	ResponseDto register(@Valid UserDto userDto);
	ResponseDto login(@Valid LoginDto loginDto);
}
