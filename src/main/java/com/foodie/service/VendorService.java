package com.foodie.service;

import com.foodie.dto.FoodItemsDto;
import com.foodie.dto.LoginDto;
import com.foodie.dto.ResponseDto;
import com.foodie.dto.VendorDto;

import jakarta.validation.Valid;

public interface VendorService {
	ResponseDto register(@Valid VendorDto vendorDto);
	ResponseDto login(@Valid LoginDto loginDto);
	ResponseDto addFoodItems(String vendorId ,FoodItemsDto foodItemsDto);

}
