package com.foodie.service;
import com.foodie.dto.OrderDto;
import com.foodie.dto.ResponseDto;
 
public interface OrderService {
    ResponseDto placeOrder(OrderDto orderDto);
  
}
