package com.foodie.service;

import com.foodie.dto.FoodItemsDto;
import com.foodie.dto.ResponseDto;
import com.foodie.entity.FoodItems;

import jakarta.validation.Valid;

public interface FoodItemsService {
	 ResponseDto addItems(@Valid FoodItemsDto foodItemsDto);

	 ResponseDto updateFood(Long foodId, FoodItemsDto foodItemsDto);

	FoodItems getFoodById(Long foodId); 
		
	
}
