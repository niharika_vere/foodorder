package com.foodie.service;

import java.util.List;

import com.foodie.entity.FoodItems;

public interface FoodSearchService {
	List<FoodItems> searchFoodItems1(String type, String foodType, String vendorName);
}
