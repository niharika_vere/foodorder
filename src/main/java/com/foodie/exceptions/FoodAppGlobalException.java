package com.foodie.exceptions;

import org.springframework.http.HttpStatus;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FoodAppGlobalException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	private String message;
    private HttpStatus httpStatus;

    public FoodAppGlobalException(String message){
        super(message);
    }
    
}