package com.foodie.repository;
 
import org.springframework.data.jpa.repository.JpaRepository;
import com.foodie.entity.Order;
 
public interface OrderRepository extends JpaRepository<Order, Long> {
   
}