package com.foodie.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodie.entity.User;

public interface LoginRepository extends JpaRepository<User, String> {

}
