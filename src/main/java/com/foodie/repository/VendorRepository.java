package com.foodie.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodie.entity.Vendor;

public interface VendorRepository extends JpaRepository<Vendor, String> {

	Optional<Vendor> findByEmail(String email);

}
