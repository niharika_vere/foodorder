package com.foodie.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.foodie.entity.FoodItems;

public interface FoodItemsRepository extends JpaRepository<FoodItems, Long> {
	List<FoodItems> findByTypeContainingIgnoreCaseAndFoodTypeAndVendorNameContainingIgnoreCase(String type, String foodType, String vendorName);
	 
	List<FoodItems> findByTypeContainingIgnoreCaseAndFoodType(String type, String foodType);
 
	List<FoodItems> findByTypeContainingIgnoreCaseAndVendorNameContainingIgnoreCase(String type, String vendorName);
 
	List<FoodItems> findByFoodTypeAndVendorNameContainingIgnoreCase(String foodType, String vendorName);
 
	List<FoodItems> findByTypeContainingIgnoreCase(String foodType);
 
	List<FoodItems> findByFoodType(String foodType);
 
	List<FoodItems> findByVendorNameContainingIgnoreCase(String vendorName);
 
}
