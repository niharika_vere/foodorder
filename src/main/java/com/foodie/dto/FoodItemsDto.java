package com.foodie.dto;


import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FoodItemsDto {
	@NotBlank(message = "foodname is requiredfield")
	private String foodName;
	
	@Min(value=5,message="price must be atleast 5 rupees")
	//@NotNull(message = "food price is requiredfield")
	private Long foodPrice;
	
	private String type;
	
	@NotBlank(message = "vendorname is requiredfield")
	private String vendorName;
	
	
	private String foodType;
}
