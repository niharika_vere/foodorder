package com.foodie.dto;
 
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
 
@AllArgsConstructor
@Data
@NoArgsConstructor
public class OrderDto {
    private Long foodId;
    private String customerId;
    private int quantity;
}
