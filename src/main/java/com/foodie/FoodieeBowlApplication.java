package com.foodie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodieeBowlApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodieeBowlApplication.class, args);
	}

}
