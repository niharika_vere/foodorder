package com.foodie.entity;

import com.foodie.dto.Roles;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@Data
@ToString
@NoArgsConstructor
@Entity
public class User {
	@Id
	private String customerId;
	private String name;
	private String email;
	private String password;
	private String contactNo;
	private boolean isLoggedin;
	@Enumerated(EnumType.STRING)
	private Roles role;


}
