package com.foodie.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.foodie.dto.OrderDto;
import com.foodie.dto.ResponseDto;
import com.foodie.dto.UserDto;
import com.foodie.entity.FoodItems;
import com.foodie.serviceImpl.FoodSearchServiceImpl;
import com.foodie.serviceImpl.OrderServiceImpl;
import com.foodie.serviceImpl.UserServiceImpl;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
@RequiredArgsConstructor
@RestController
@RequestMapping("/user")
public class UserController {
	private final OrderServiceImpl orderServiceImpl;
	private final UserServiceImpl userServiceImpl;
	private final FoodSearchServiceImpl foodServiceImpl;
	
	@PostMapping("/")
	public ResponseEntity<ResponseDto> register(@Valid @RequestBody UserDto userDto){
		return new ResponseEntity<ResponseDto>(userServiceImpl.register(userDto),HttpStatus.CREATED);
	}

    @PostMapping("/order")
    public ResponseEntity<ResponseDto> placeOrder(@RequestBody OrderDto orderDto) {
        ResponseDto responseDto = orderServiceImpl.placeOrder(orderDto);
        return new ResponseEntity<>(responseDto, HttpStatus.valueOf(responseDto.getHttpCode()));
    }
    @GetMapping("/")
    public ResponseEntity<List<FoodItems>> searchFoodItems(
            @RequestParam(required = false) String type,
            @RequestParam(required = false) String foodType,
            @RequestParam(required = false) String vendor) {
        List<FoodItems> foodItems = foodServiceImpl.searchFoodItems1(type, foodType, vendor);
        return new ResponseEntity<>(foodItems, HttpStatus.OK);
    }

}
