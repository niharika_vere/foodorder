package com.foodie.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.foodie.dto.FoodItemsDto;
import com.foodie.dto.ResponseDto;
import com.foodie.dto.VendorDto;
import com.foodie.serviceImpl.FoodItemsServiceImpl;
import com.foodie.serviceImpl.VendorServiceImpl;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/vendor")
public class VendorController {
	private final VendorServiceImpl vendorServiceImpl;
	private final FoodItemsServiceImpl foodItemsServiceImpl;

	@PostMapping("/")
	public ResponseEntity<ResponseDto> register(@Valid @RequestBody VendorDto vendorDto) {
		return new ResponseEntity<ResponseDto>(vendorServiceImpl.register(vendorDto), HttpStatus.CREATED);
	}

	@PostMapping("/{vendorId}/")
	public ResponseEntity<ResponseDto> addFoodItem(@PathVariable String vendorId,
			@Valid @RequestBody FoodItemsDto foodItemDto) {
		return new ResponseEntity<>(vendorServiceImpl.addFoodItems(vendorId, foodItemDto), HttpStatus.CREATED);
	}

	@PutMapping("/{foodId}/")
	public ResponseEntity<ResponseDto> updatefood(@PathVariable Long foodId, @RequestBody FoodItemsDto foodItemDto) {
		return new ResponseEntity<ResponseDto>(foodItemsServiceImpl.updateFood(foodId, foodItemDto), HttpStatus.OK);
	}

	@DeleteMapping("/{foodId}/")
	public ResponseEntity<ResponseDto> deletefood(@PathVariable Long foodId) {
		return new ResponseEntity<ResponseDto>(foodItemsServiceImpl.deleteFood(foodId), HttpStatus.OK);
	}

}
