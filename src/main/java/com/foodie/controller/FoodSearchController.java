//package com.foodie.controller;
//
//import java.util.List;
//
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.foodie.entity.FoodItems;
//import com.foodie.serviceImpl.FoodSearchServiceImpl;
//
//import lombok.RequiredArgsConstructor;
//
//@RequiredArgsConstructor
//@RestController
//@RequestMapping("/foods")
//public class FoodSearchController {
//	private final FoodSearchServiceImpl foodServiceImpl;
//	@GetMapping
//    public ResponseEntity<List<FoodItems>> searchFoodItems(
//            @RequestParam(required = false) String type,
//            @RequestParam(required = false) String foodType,
//            @RequestParam(required = false) String vendor) {
//        List<FoodItems> foodItems = foodServiceImpl.searchFoodItems1(type, foodType, vendor);
//        return new ResponseEntity<>(foodItems, HttpStatus.OK);
//    }
//}
