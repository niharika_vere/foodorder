package com.foodie.controller;
 
import com.foodie.dto.LoginDto;
import com.foodie.dto.ResponseDto;
import com.foodie.service.UserService;
import com.foodie.service.VendorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
 
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
 
@RequiredArgsConstructor
@RestController
@RequestMapping("/login")
public class LoginController {
 
    private final UserService userService;
    private final VendorService vendorService;
 
    @PostMapping("/")
    public ResponseEntity<ResponseDto> login(@Valid @RequestBody LoginDto loginDto) {
        if (loginDto.getId().startsWith("VID")) {
            return new ResponseEntity<>(vendorService.login(loginDto), HttpStatus.OK);
        } else if (loginDto.getId().startsWith("CID")) {   
            return new ResponseEntity<>(userService.login(loginDto), HttpStatus.OK);
        } else { 
            return new ResponseEntity<>(ResponseDto.builder().httpCode(400).message("Invalid login request").build(), HttpStatus.BAD_REQUEST);
        }
    }
}