//package com.foodie.controller;
// 
//import com.foodie.dto.OrderDto;
//import com.foodie.dto.ResponseDto;
//import com.foodie.service.OrderService;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
// 
//@RestController
//@RequestMapping("/api/orders")
//public class OrderController {
// 
//    private final OrderService orderService;
// 
//    public OrderController(OrderService orderService) {
//        this.orderService = orderService;
//    }
// 
//    @PostMapping("/place")
//    public ResponseEntity<ResponseDto> placeOrder(@RequestBody OrderDto orderDto) {
//        ResponseDto responseDto = orderService.placeOrder(orderDto);
//        return new ResponseEntity<>(responseDto, HttpStatus.valueOf(responseDto.getHttpCode()));
//    }
//}