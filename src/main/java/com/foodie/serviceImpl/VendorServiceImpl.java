	package com.foodie.serviceImpl;

import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import com.foodie.dto.FoodItemsDto;
import com.foodie.dto.LoginDto;
import com.foodie.dto.ResponseDto;
import com.foodie.dto.Roles;
import com.foodie.dto.VendorDto;
import com.foodie.entity.FoodItems;
import com.foodie.entity.Vendor;
import com.foodie.exceptions.UserAlreadyExists;
import com.foodie.repository.FoodItemsRepository;
import com.foodie.repository.VendorRepository;
import com.foodie.service.VendorService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class VendorServiceImpl implements VendorService {
	private final VendorRepository vendorRepository;
	private final FoodItemsRepository foodItemsRepository;

	public ResponseDto register(@Valid VendorDto vendorDto) {
		Optional<Vendor> vendor = vendorRepository.findByEmail(vendorDto.getEmail());
		if (vendor.isPresent()) {
			throw new UserAlreadyExists("User already exists");
		}
		Vendor register = new Vendor();
		register.setVendorname(vendorDto.getVendorname());
		register.setEmail(vendorDto.getEmail());
		register.setContactNo(vendorDto.getContactNo());
		register.setLoggedin(false);
		register.setRole(Roles.VENDOR);
		String characters = "0123456789";
		String id = RandomStringUtils.random(6, characters);
		String id2 = "VID" + id;
		register.setVendorId(id2);
		String characters2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		String pwd = RandomStringUtils.random(8, characters2);
		register.setPassword(pwd);
		vendorRepository.save(register);
		return ResponseDto.builder().httpCode(201).message("Registered successfully").build();
	}

	public ResponseDto login(LoginDto loginDto) {
	    Optional<Vendor> vendorOptional = vendorRepository.findById(loginDto.getId());
	 
	    if (vendorOptional.isPresent()) {
	        Vendor vendor = vendorOptional.get();
	        if (vendor.getPassword().equals(loginDto.getPassword())) {
	            vendor.setLoggedin(true);
	            vendorRepository.save(vendor); // Save the updated entity
	            return ResponseDto.builder().httpCode(200).message("Vendor login successful").build();
	        } else {
	            return ResponseDto.builder().httpCode(401).message("Invalid credentials").build();
	        }
	    } else {
	        // Handle vendor not found
	        return ResponseDto.builder().httpCode(404).message("Vendor not found").build();
	    }
	}

	public ResponseDto addFoodItems(String vendorId, FoodItemsDto foodItemsDto) {
		Optional<Vendor> vendor = vendorRepository.findById(vendorId);

		if (vendor.isPresent()) {
			if (vendor.get().getRole() == Roles.VENDOR) {

				FoodItems additems = new FoodItems();
				additems.setVendor(vendor.get());
				additems.setFoodName(foodItemsDto.getFoodName());
				additems.setFoodPrice(foodItemsDto.getFoodPrice());
				additems.setFoodType(foodItemsDto.getFoodType());
				additems.setType(foodItemsDto.getType());
				additems.setVendorName(foodItemsDto.getVendorName());
				foodItemsRepository.save(additems);
				return ResponseDto.builder().httpCode(201).message("Food item added successfully").build();
			} else {
				return ResponseDto.builder().httpCode(403).message("Vendor is not authorized to add food items")
						.build();
			}
		} else {
			return ResponseDto.builder().httpCode(404).message("Vendor not found").build();
		}
	}

}
