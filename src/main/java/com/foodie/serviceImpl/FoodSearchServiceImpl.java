package com.foodie.serviceImpl;
 
import java.util.List;
 
import org.springframework.stereotype.Service;
 
import com.foodie.entity.FoodItems;
import com.foodie.repository.FoodItemsRepository;
import com.foodie.service.FoodSearchService;
 
import lombok.RequiredArgsConstructor;
 
@RequiredArgsConstructor
@Service
public class FoodSearchServiceImpl implements FoodSearchService {
 
    private final FoodItemsRepository foodItemsRepository;
    @Override
    public List<FoodItems> searchFoodItems1(String type, String foodType, String vendorName) {
        if (type != null && foodType != null && vendorName != null) {
            return foodItemsRepository.findByTypeContainingIgnoreCaseAndFoodTypeAndVendorNameContainingIgnoreCase(type, foodType,vendorName);
        } else if (type != null && foodType != null) {
        	return foodItemsRepository.findByTypeContainingIgnoreCaseAndFoodType(type, foodType);
        } else if (type != null && vendorName != null) {
        	return foodItemsRepository.findByTypeContainingIgnoreCaseAndVendorNameContainingIgnoreCase(foodType, vendorName);
        } else if (foodType != null && vendorName != null) {
        	return foodItemsRepository.findByFoodTypeAndVendorNameContainingIgnoreCase(foodType, vendorName);
        } else if (type != null) {
            return foodItemsRepository.findByTypeContainingIgnoreCase(type);
        } else if (foodType != null) {
           return foodItemsRepository.findByFoodType(foodType);
        } else if (vendorName != null) {
            return foodItemsRepository.findByVendorNameContainingIgnoreCase(vendorName);
        } else {
       
            return foodItemsRepository.findAll();
        }
    }
}