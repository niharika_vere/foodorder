package com.foodie.serviceImpl;
 
import com.foodie.dto.OrderDto;
import com.foodie.dto.ResponseDto;
import com.foodie.entity.FoodItems;
import com.foodie.entity.Order;
import com.foodie.repository.OrderRepository;
import com.foodie.service.FoodItemsService;
import com.foodie.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
@Service
public class OrderServiceImpl implements OrderService {
 
    private final OrderRepository orderRepository;
    private final FoodItemsService foodItemsService;
 
    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, FoodItemsService foodItemsService) {
        this.orderRepository = orderRepository;
        this.foodItemsService = foodItemsService;
    }
 
    @Override
    public ResponseDto placeOrder(OrderDto orderDto) {
        
        if (orderDto.getQuantity() <= 0) {
            return ResponseDto.builder().httpCode(400).message("Invalid quantity").build();
        }
 
        
        FoodItems foodItem = foodItemsService.getFoodById(orderDto.getFoodId());
        if (foodItem == null) {
            return ResponseDto.builder().httpCode(404).message("Food item not found").build();
        }
 
     
        double totalAmount = orderDto.getQuantity() * foodItem.getFoodPrice();
 
        Order order = new Order();
        order.setFoodId(orderDto.getFoodId());
        order.setCustomerId(orderDto.getCustomerId());
        order.setQuantity(orderDto.getQuantity());
        order.setTotalAmount(totalAmount);
        orderRepository.save(order);
        return ResponseDto.builder().httpCode(201).message("Order placed successfully").build();
    }
}