package com.foodie.serviceImpl;

import org.springframework.stereotype.Service;

import com.foodie.dto.FoodItemsDto;
import com.foodie.dto.ResponseDto;
import com.foodie.entity.FoodItems;
import com.foodie.entity.Vendor;
import com.foodie.exceptions.ResourceConflictExists;
import com.foodie.exceptions.ResourceNotFound;
import com.foodie.repository.FoodItemsRepository;
import com.foodie.repository.VendorRepository;
import com.foodie.service.FoodItemsService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class FoodItemsServiceImpl implements FoodItemsService {

	private final FoodItemsRepository foodItemsRepository;
	private final VendorRepository VendorRepository;

	public ResponseDto addItems(@Valid FoodItemsDto foodItemsDto) {
		FoodItems additems = new FoodItems();
		additems.setFoodName(foodItemsDto.getFoodName());
		additems.setFoodPrice(foodItemsDto.getFoodPrice());
		additems.setFoodType(foodItemsDto.getFoodType());
		additems.setType(foodItemsDto.getType());
		additems.setVendorName(foodItemsDto.getVendorName());
		foodItemsRepository.save(additems);
		return ResponseDto.builder().httpCode(201).message("Items Added Successfully").build();
	}

	public ResponseDto updateFood(Long foodId, FoodItemsDto foodItemsDto) {
		FoodItems foodDetails = foodItemsRepository.findById(foodId)
				.orElseThrow(() -> new ResourceNotFound("Food not found"));

		Vendor vendorRegistartion = foodDetails.getVendor();
		if (vendorRegistartion == null || !vendorRegistartion.isLoggedin()) {
			throw new ResourceConflictExists("Vendor should be logged in");
		}

		foodDetails.setFoodName(foodItemsDto.getFoodName());
		foodDetails.setFoodPrice(foodItemsDto.getFoodPrice());

		foodItemsRepository.save(foodDetails);

		return ResponseDto.builder().httpCode(201).message("Food details updated successfully").build();
	}

	public ResponseDto deleteFood(Long foodId) {
		FoodItems foodDetails = foodItemsRepository.findById(foodId)
			            .orElseThrow(() -> new ResourceNotFound("Food not found"));
		 
		Vendor vendorRegistartion = foodDetails.getVendor();
			    if (vendorRegistartion == null || !vendorRegistartion.isLoggedin()) {
			        throw new ResourceConflictExists("Vendor should be logged in");
			    }
		 
			    foodItemsRepository.deleteById(foodId);
		 
			    return ResponseDto.builder().httpCode(201).message("Food deleted successfully").build();
			}

	@Override
	public FoodItems getFoodById(Long foodId) {
		
		return null;
	}

}
