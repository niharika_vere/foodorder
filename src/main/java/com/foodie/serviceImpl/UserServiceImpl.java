package com.foodie.serviceImpl;

import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import com.foodie.dto.LoginDto;
import com.foodie.dto.ResponseDto;
import com.foodie.dto.Roles;
import com.foodie.dto.UserDto;
import com.foodie.entity.User;
import com.foodie.exceptions.UserAlreadyExists;
import com.foodie.repository.UserRepository;
import com.foodie.service.UserService;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
@AllArgsConstructor
@Service
public class UserServiceImpl implements UserService{
	public final UserRepository userRepository;
	 

	public ResponseDto register(@Valid UserDto userDto) {
		Optional<User> user1=userRepository.findByEmail(userDto.getEmail());
		if(user1.isPresent()) {
			throw new UserAlreadyExists("User already exists");
		}
		User user=new User();
		String characters = "0123456789";
		String id = RandomStringUtils.random(6, characters);
		String id2="CID"+id;
		user.setCustomerId(id2);
		user.setName(userDto.getName());
		user.setEmail(userDto.getEmail());
		user.setContactNo(userDto.getContactNo());
		String characters2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		String pwd = RandomStringUtils.random(8, characters2);
		user.setPassword(pwd);
		user.setLoggedin(false);
		user.setRole(Roles.USER);
		userRepository.save(user);
		return ResponseDto.builder().httpCode(201).message("Registered successfully").build();
	}

	
	
	public ResponseDto login(LoginDto loginDto) {
	    Optional<User> userOptional = userRepository.findById(loginDto.getId());
	 
	    if (userOptional.isPresent()) {
	        User user = userOptional.get();
	        if (user.getPassword().equals(loginDto.getPassword())) {
	            user.setLoggedin(true);
	            userRepository.save(user); // Save the updated entity
	            return ResponseDto.builder().httpCode(200).message("User login successful").build();
	        } else {
	            return ResponseDto.builder().httpCode(401).message("Invalid credentials").build();
	        }
	    } else {
	        return ResponseDto.builder().httpCode(404).message("User not found").build();
	    }
	}

}
